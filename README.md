[![Documentation Status](https://readthedocs.org/projects/nob/badge/?version=latest)](https://nob.readthedocs.io/en/latest/?badge=latest)

# nob: the Nested OBject manipulator

Deep nested data is sometimes difficult to map with objects in an Object
Relational Mapping (ORM) appraoch. Suppose *e.g.* you're working with this file:

```yaml
root:
  System:
    Library:
      Frameworks:
        Python.framework:
          Versions:
            2.7:
              bin: python2.7
```

If you wanted the last value here using pure Python, you would have to write something like:

```python
pure_python['root']['System']['Library']['Frameworks']['Python.framework']['Versions']['2.7']['bin']
```

Gosh that's bad, it doesn't even fit in the screen!  This is where nob shines:
it offers a simple set of tools to explore and edit any nested data (Python
native dicts and lists). With nob, this becomes:

```python
nob_object['bin'][:]
```

Pretty neat, no? For more, checkout nob's
[documentation](https://nob.readthedocs.io/).

# Installation

    pip install nob

Enough said :)