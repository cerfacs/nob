import json

import numpy as np
import pytest

from nob import Nob, Path


@pytest.fixture(scope='function')
def simple_nob():
    return Nob({
        'key1': 'val1',
        'key2': {
            'key3': 4,
            'key4': {'key5': 'val2'},
            'key5': [3, 4, 5]
            },
        'key5': 'val3'
        })


def test_paths(simple_nob):
    paths = simple_nob.paths
    assert all(isinstance(p, Path) for p in paths)
    assert paths == [
        Path('/'),
        Path('/key1'),
        Path('/key2'),
        Path('/key2/key3'),
        Path('/key2/key4'),
        Path('/key2/key4/key5'),
        Path('/key2/key5'),
        Path('/key2/key5/0'),
        Path('/key2/key5/1'),
        Path('/key2/key5/2'),
        Path('/key5')]

    paths = simple_nob.key2.paths
    assert all(isinstance(p, Path) for p in paths)
    assert paths == [
        Path('/'),
        Path('/key3'),
        Path('/key4'),
        Path('/key4/key5'),
        Path('/key5'),
        Path('/key5/0'),
        Path('/key5/1'),
        Path('/key5/2')]


def test_leaves(simple_nob):
    leaves = simple_nob.leaves
    assert all(isinstance(p, Path) for p in leaves)
    assert leaves == [
        Path('/key1'),
        Path('/key2/key3'),
        Path('/key2/key4/key5'),
        Path('/key2/key5/0'),
        Path('/key2/key5/1'),
        Path('/key2/key5/2'),
        Path('/key5')]

    leaves = simple_nob.key2.leaves
    assert all(isinstance(p, Path) for p in leaves)
    assert leaves == [
        Path('/key3'),
        Path('/key4/key5'),
        Path('/key5/0'),
        Path('/key5/1'),
        Path('/key5/2')]


def test_find(simple_nob):
    fin = simple_nob.find
    assert fin('/key1') == [Path('/key1')]
    assert fin(Path('/key1')) == [Path('/key1')]
    assert fin('key1') == [Path('/key1')]
    assert fin('key3') == [Path('/key2/key3')]
    assert fin('key4') == [Path('/key2/key4')]
    assert fin('key5') == [Path('/key2/key4/key5'),
                           Path('/key2/key5'),
                           Path('/key5')]
    assert fin('0') == [Path('/key2/key5/0')]
    assert fin(0) == [Path('/key2/key5/0')]
    assert fin('foo') == []
    assert fin('/foo') == []


def test_find_unique(simple_nob):
    with pytest.raises(KeyError):
        simple_nob._find_unique('key5')
    assert simple_nob._find_unique('key3') == Path('/key2/key3')


def test_slice_trick_access():
    nob = Nob({'key!': 'val1'})
    assert nob[:] == {'key!': 'val1'}
    nob = Nob({'superkey': {'key!': 'val1'}})
    assert nob.superkey[:] == {'key!': 'val1'}


def test_real_slice_access(simple_nob):
    with pytest.raises(TypeError):
        simple_nob[1:]

    assert simple_nob.key1[:-1] == 'val'

    nv_list = simple_nob['/key2/key5'][1:]
    assert [n[:] for n in nv_list] == [4, 5]

    with pytest.raises(TypeError):
        simple_nob[1:]


def test_index_access(simple_nob):
    with pytest.raises(KeyError):
        simple_nob[1]

    assert simple_nob.key1[-1] == '1'

    nv = simple_nob['/key2/key5']
    assert nv[1][:] == 4
    assert nv['1'][:] == 4
    assert nv[:2] == [nv[0], nv[1]]

    # It also needs to work for dicts
    # BUT, according to JSON, only strings are acceptable keys
    simple_nob[0] = '0'
    simple_nob['1'] = '1'
    assert simple_nob['1'][:] == '1'
    assert simple_nob['0'][:] == '0'


def test_convert_dict_integer_key():
    n = Nob()
    n[1] = '1'
    assert n[:] == {'1': '1'}


def test_unauthorized_access(simple_nob):
    with pytest.raises(TypeError):
        simple_nob[simple_nob]


def test_reserved_access():
    nb = Nob({'level0': {
        'data': 1,
        'root': 1,
        'tree': 1,
        '_data': 1,
        '_root': 1,
        '_tree': 1,
    }})
    for n in [nb, nb['level0']]:
        assert n['data'][:] == 1
        assert n['_data'][:] == 1
        assert n['root'][:] == 1
        assert n['_root'][:] == 1
        assert n['tree'][:] == 1
        assert n['_tree'][:] == 1

        assert n.data[:] == 1
        with pytest.raises(TypeError):
            n._data[:]
        assert type(n.root) == Path
        assert type(n._root) == Path
        assert n.tree[:] == 1
        assert type(n._tree) == Nob


def test_reserved_assign(simple_nob):
    with pytest.raises(AttributeError):
        simple_nob._data = None
    with pytest.raises(AttributeError):
        simple_nob._root = None
    with pytest.raises(AttributeError):
        simple_nob._nob = None
    with pytest.raises(AttributeError):
        simple_nob.root = None
    with pytest.raises(AttributeError):
        simple_nob.paths = None
    with pytest.raises(AttributeError):
        simple_nob.val = None


def test_raw_data(simple_nob):
    nob = Nob({'key!': 'val1'})
    assert nob._raw_data(Path('/')) == {'key!': 'val1'}
    assert nob._raw_data(Path('/key!')) == 'val1'


def test_global_access(simple_nob):
    nv = simple_nob['/key1']
    assert nv._root == Path('/key1')
    assert nv[:] == 'val1'
    nv = simple_nob[Path('/key1')]
    assert nv._root == Path('/key1')
    assert nv[:] == 'val1'

    nv = simple_nob['/key2/key3']
    assert nv._root == Path('/key2/key3')
    assert nv[:] == 4
    nv = simple_nob[Path('/key2/key3')]
    assert nv._root == Path('/key2/key3')
    assert nv[:] == 4

    nv = simple_nob['/key2/key4']
    assert nv._root == Path('/key2/key4')
    assert nv[:] == {'key5': 'val2'}
    nv = simple_nob[Path('/key2/key4')]
    assert nv._root == Path('/key2/key4')
    assert nv[:] == {'key5': 'val2'}

    nv = simple_nob['/key2/key5/0']
    assert nv._root == Path('/key2/key5/0')
    assert nv[:] == 3
    nv = simple_nob[Path('/key2/key5/0')]
    assert nv._root == Path('/key2/key5/0')
    assert nv[:] == 3

    nv = simple_nob['/key5']
    assert nv._root == Path('/key5')
    assert nv[:] == 'val3'
    nv = simple_nob[Path('/key5')]
    assert nv._root == Path('/key5')
    assert nv[:] == 'val3'


def test_key_access(simple_nob):
    nv = simple_nob['key1']
    assert nv._root == Path('/key1')
    assert nv[:] == 'val1'

    nv = simple_nob['key3']
    assert nv._root == Path('/key2/key3')
    assert nv[:] == 4

    nv = simple_nob['key4']['key5']
    assert nv._root == Path('/key2/key4/key5')
    assert nv[:] == 'val2'

    with pytest.raises(KeyError):
        nv = simple_nob[0]


def test_attribute_access(simple_nob):
    nv = simple_nob.key1
    assert nv._root == Path('/key1')
    assert nv[:] == 'val1'

    nv = simple_nob.key3
    assert nv._root == Path('/key2/key3')
    assert nv[:] == 4

    nv = simple_nob.key4.key5
    assert nv._root == Path('/key2/key4/key5')
    assert nv[:] == 'val2'

# This is not supported yet
# def test_dynamic_access(simple_nob):
#    nv = simple_nob['/*/key4']
#    assert isinstance(nv, NobView)
#    assert nv._root == Path('/key2/key4')
#    assert nv._raw_data() == 4


def test_assign(simple_nob):
    simple_nob.key1 = 'new_val'
    assert simple_nob.key1[:] == 'new_val'
    assert simple_nob[:]['key1'] == 'new_val'

    nv = simple_nob.key2
    nv['key3'] = 3
    assert simple_nob['/key2/key3'][:] == 3
    assert nv.key3[:] == 3

    nv = simple_nob.key2['/key5']
    nv[0] = 0
    assert nv[:] == [0, 4, 5]
    assert simple_nob['/key2/key5'][:] == [0, 4, 5]
    nv['1'] = 0
    assert nv[:] == [0, 0, 5]
    assert simple_nob['/key2/key5'][:] == [0, 0, 5]

    simple_nob['/key2/key4/key5'] = 'new_val'
    assert simple_nob['/key2/key4/key5'][:] == 'new_val'
    simple_nob[Path('/key2/key4/key5')] = 'newer_val'
    assert simple_nob['/key2/key4/key5'][:] == 'newer_val'

    with pytest.raises(AttributeError):
        simple_nob.new_key = 'new_val'


def test_set(simple_nob):
    nv = simple_nob.key1
    nv.set('new_val')
    assert simple_nob.key1[:] == 'new_val'
    assert nv[:] == 'new_val'

    simple_nob.set({'new_tree': 1})
    assert simple_nob[:] == {'new_tree': 1}


def test_len(simple_nob):
    assert len(simple_nob) == 3
    assert len(simple_nob.key2) == 3
    assert len(simple_nob['/key2/key5']) == 3


def test_iter(simple_nob):
    children = [c for c in simple_nob]
    assert children == [simple_nob['/key1'], simple_nob['/key2'], simple_nob['/key5']]
    children = [c for c in simple_nob['key2']]
    assert children == [simple_nob['/key2/key3'], simple_nob['/key2/key4'], simple_nob['/key2/key5']]
    list_tree = simple_nob['/key2/key5']
    children = [c for c in list_tree]
    assert children == [list_tree[0], list_tree[1], list_tree[2]]


def test_equals(simple_nob):
    nv1 = simple_nob.key1
    t2 = Nob('val1')
    assert nv1 == t2


def test_copy(simple_nob):
    new_nob = simple_nob.copy()
    assert new_nob[:] == simple_nob[:]
    assert new_nob is not simple_nob
    new_nob.key1 = 'new'
    assert new_nob.key1[:] != simple_nob.key1[:]

    small = simple_nob.key2.copy()
    assert small[:] == simple_nob.key2[:]
    small.key3 = 5
    assert small.key3[:] != simple_nob.key3[:]


def test_root(simple_nob):
    full_path = simple_nob.key3.root
    assert full_path == Path('/key2/key3')
    with pytest.raises(AttributeError):
        simple_nob.key3.root = Path('/')


def test_parent(simple_nob):
    nv = simple_nob.key3
    assert nv.parent == simple_nob.key2
    assert simple_nob.key1.parent == simple_nob
    with pytest.raises(IndexError):
        simple_nob.parent


def test_keys(simple_nob):
    assert list(simple_nob.keys()) == ['key1', 'key2', 'key5']
    assert list(simple_nob.key2.keys()) == ['key3', 'key4', 'key5']
    assert list(simple_nob['/key2/key5'].keys()) == [0, 1, 2]


def test_add_key(simple_nob):
    simple_nob['new_key'] = 'new_val'
    assert 'new_key' in simple_nob[:]
    assert simple_nob[:]['new_key'] == 'new_val'
    simple_nob.key2['new_key'] = 'new_val'
    assert 'new_key' in simple_nob.key2[:]
    assert simple_nob.key2[:]['new_key'] == 'new_val'

    simple_nob['/new_key2'] = 'new_val'
    assert 'new_key2' in simple_nob[:]
    assert simple_nob[:]['new_key2'] == 'new_val'
    simple_nob.key2['/new_key2'] = 'new_val'
    assert 'new_key2' in simple_nob.key2[:]
    assert simple_nob.key2[:]['new_key2'] == 'new_val'
    simple_nob['/key2/new_key3'] = 'new_val'
    assert 'new_key3' in simple_nob.key2[:]
    assert simple_nob.key2[:]['new_key3'] == 'new_val'

    with pytest.raises(AttributeError):
        simple_nob.new_key4 = 'new_val'
    with pytest.raises(AttributeError):
        simple_nob.key2.new_key4 = 'new_val'


def test_del_attribute(simple_nob):
    del simple_nob.key1
    assert list(simple_nob.keys()) == ['key2', 'key5']
    del simple_nob['/key5']
    assert list(simple_nob.keys()) == ['key2']
    del simple_nob['key3']
    assert list(simple_nob.key2.keys()) == ['key4', 'key5']
    nv = simple_nob.key2
    del nv['/key5']
    assert list(simple_nob.key2.keys()) == ['key4']
    save = simple_nob.copy()
    del nv
    assert simple_nob == save


def test_del_list(simple_nob):
    del simple_nob['/key2/key5'][0]
    assert simple_nob['/key2/key5'][:] == [4, 5]
    del simple_nob['/key2/key5'][:1]
    assert simple_nob['/key2/key5'][:] == [5]


def test_del_val(simple_nob):
    del simple_nob['/key2/key5'][:]
    assert simple_nob['/key2/key5'][:] == None
    del simple_nob['/key1'][:]
    assert simple_nob['/key1'][:] == None


def test_get(simple_nob):
    assert simple_nob.get('no_key') is None
    assert simple_nob.get('key1') == 'val1'
    assert simple_nob.key2.get('key4') == {'key5': 'val2'}


def test_np_serialize_and_deserialize(tmp_path, simple_nob):
    ntest = simple_nob.copy()

    ntest['key_np'] = np.arange(3)
    ntest.np_serialize()

    for key in 'key1 key2 /key5'.split():
        assert ntest[key][:] == simple_nob[key][:]
    assert type(ntest['key_np'][:]) == str

    # Test that the result can be written to disc
    with open(tmp_path / 'file.json', 'w') as fh:
        json.dump(ntest[:], fh)
    with open(tmp_path / 'file.json') as fh:
        ntest = Nob(json.load(fh))

    ntest.np_deserialize()
    for key in 'key1 key2 /key5'.split():
        assert ntest[key][:] == simple_nob[key][:]
    assert np.allclose(ntest['key_np'][:], np.arange(3))


def test_np_serialize_and_deserialize_compressed(tmp_path, simple_nob):
    ntest = simple_nob.copy()

    ntest['key_np'] = np.arange(3)
    ntest.np_serialize(compress=True)

    for key in 'key1 key2 /key5'.split():
        assert ntest[key][:] == simple_nob[key][:]
    assert type(ntest['key_np'][:]) == str

    # Test that the result can be written to disc
    with open(tmp_path / 'file.json', 'w') as fh:
        json.dump(ntest[:], fh)
    with open(tmp_path / 'file.json') as fh:
        ntest = Nob(json.load(fh))

    ntest.np_deserialize()
    for key in 'key1 key2 /key5'.split():
        assert ntest[key][:] == simple_nob[key][:]
    assert np.allclose(ntest['key_np'][:], np.arange(3))
