import operator

import numpy as np
import pytest

from nob import Nob, Path
from nob.tools import _allclose_compare, allclose, diff


@pytest.fixture(scope="function")
def simple_nob():
    return Nob(
        {
            "str": "val1",
            "dict": {
                "int": 1,
                "float": 1.0,
                "ndarray": np.arange(3),
                "bool": True,
            },
        }
    )


def test_allclose_compare_simple():
    assert _allclose_compare("a", "a")
    assert not _allclose_compare("a", "A")

    assert _allclose_compare(1, 1.0)
    assert _allclose_compare(True, 1.0)
    assert _allclose_compare(False, 0.0)

    assert _allclose_compare(np.arange(3), np.arange(3).astype(float))

    assert _allclose_compare(None, None)
    assert not _allclose_compare(1, None)
    assert not _allclose_compare("a", None)


def test_allclose_compare_tolerances():
    assert _allclose_compare(0, 1e-8)
    assert not _allclose_compare(0, 2e-8)
    assert _allclose_compare(0, 2e-8, atol=1e-7)

    assert _allclose_compare(1, 1 + 1e-5)
    assert not _allclose_compare(1, 1 + 2e-5)
    assert _allclose_compare(1, 1 + 2e-5, rtol=1e-4)


def test_allclose_simple(simple_nob):
    shadow = simple_nob.copy()
    assert allclose(simple_nob, shadow)

    shadow.dict.int = True
    assert allclose(simple_nob, shadow)

    shadow.dict.float = 1
    assert allclose(simple_nob, shadow)

    shadow.dict.bool = 1
    assert allclose(simple_nob, shadow)

    shadow.dict.ndarray = np.arange(3).astype(float)
    assert allclose(simple_nob, shadow)


def test_allclose_tolerances(simple_nob):
    shadow = simple_nob.copy()
    shadow.dict.float = 1 + 1e-5
    assert allclose(simple_nob, shadow)

    shadow.dict.float = 1 + 1e-4
    assert not allclose(simple_nob, shadow)
    assert allclose(simple_nob, shadow, rtol=1e-3)


def test_diff(simple_nob):
    shadow = simple_nob.copy()
    assert diff(simple_nob, shadow) == ([], [], [])

    shadow["new"] = 1
    assert diff(simple_nob, shadow) == ([], [], [Path("/new")])
    assert diff(shadow, simple_nob) == ([Path("/new")], [], [])

    shadow = simple_nob.copy()
    shadow.dict.float = 1 + 1e-5
    assert diff(simple_nob, shadow) == ([], [], [])

    shadow.dict.int = 0
    assert diff(simple_nob, shadow) == ([], [Path("/dict/int")], [])

    shadow.dict.float = 1 + 2e-5
    bef, dif, aft = diff(simple_nob, shadow)
    assert (bef, sorted(dif), aft) == (
        [],
        sorted([Path("/dict/int"), Path("/dict/float")]),
        [],
    )  # `sorted` necessary because order not guaranteed

    assert diff(simple_nob, shadow, rtol=1e-4) == ([], [Path("/dict/int")], [])
    assert diff(simple_nob, shadow, rtol=1e-4, atol=2) == ([], [], [])


def test_diff_eq_operator(simple_nob):
    del simple_nob.dict.ndarray  # Can't use == on np.ndarrays
    shadow = simple_nob.copy()
    assert diff(simple_nob, shadow, operator.eq) == ([], [], [])
