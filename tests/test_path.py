import pytest

from nob import Path


def test_default_and_string():
    p = Path()
    assert str(p) == '/'
    assert p.data == []


def test_init():
    p = Path('/dir/subdir')
    p2 = Path(p)
    assert p == p2
    p3 = Path(p.data)
    assert p == p3
    with pytest.raises(TypeError):
        Path('dir')


def test_divide():
    p = Path()
    p2 = p / 'dir'
    assert isinstance(p2, Path)
    assert str(p2) == '/dir'

    p3 = p / Path('/dir')
    assert isinstance(p3, Path)
    assert str(p3) == '/dir'

    p4 = p / Path('/')
    assert isinstance(p4, Path)
    assert str(p4) == '/'
    assert p4.data == []


def test_iter():
    for k in Path('/'):
        assert False

    assert [k for k in Path('/dir')] == ['dir']
    assert [k for k in Path('/dir/subdir')] == 'dir subdir'.split()


def test_contains():
    assert 'dir' in Path('/dir/subdir')


def test_equals():
    p = Path('/dir/subdir')
    p2 = Path('/dir/subdir')
    assert p == p2
    assert p is not p2


def test_getitem():
    p = Path('/dir/subdir')
    assert p[0] == 'dir'
    assert p[-1] == 'subdir'


def test_setitem():
    p = Path('/dir/subdir')
    p[-1] = 'new'
    assert p[-1] == 'new'


def test_hash():
    p = (Path('/dir'), Path('/dir/subdir'))
    assert set(p) == {Path('/dir'), Path('/dir/subdir')}


def test_parent():
    p = Path('/dir')
    assert isinstance(p.parent, Path)
    assert str(p.parent) == '/'

    p = Path('/dir/subdir')
    assert isinstance(p.parent, Path)
    assert str(p.parent) == '/dir'

    p = Path()
    with pytest.raises(IndexError):
        p.parent


def test_startswith():
    p = Path('/dir')
    p2 = Path('/dir/subdir')
    assert p2.startswith(p)
    assert not p.startswith(p2)


def test_split():
    assert Path('/dir/subdir').split() == (Path('/dir'), 'subdir')
    assert Path('/dir').split() == (Path('/'), 'dir')
    with pytest.raises(TypeError):
        Path('/').split()
