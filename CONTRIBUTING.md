# How to contribute to NOB

Thank you for considering contributing to NOB!

## Support questions

Please don't use issues for support questions. If you're in trouble, you can:

  - Search the [documentation](http://nob.rtfd.io/).
  - Ping me on Slack or Basecamp if you're part of [CERFACS](https://cerfacs.fr).
  - Shoot me [an email](mailto:lapeyre@cerfacs.fr) otherwise.

## Reporting issues

Please follow this checklist in your issue post:

  - Report the Python and nob version you are using (reminder:
    `python -c 'import nob; print(nob.__version__)'`).
  - Describe what you expected to happen. If applicable, link to a resource
    that behaves in a way that supports this intuition.
  - Consider adding a [minimal reproducible example](https://stackoverflow.com/help/minimal-reproducible-example).
  - Tell us what actually happened, and include a relevant traceback if an
    error was raised.
