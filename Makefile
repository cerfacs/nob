wheel:
	\rm -rf dist/*
	python setup.py bdist_wheel

test_upload: wheel
	cat ~/.pypi.cfg
	twine upload --repository-url https://test.pypi.org/legacy/ dist/*
	echo "Remember to test in new env with: pip install --no-cache-dir --index-url https://test.pypi.org/simple/ nob"

upload: wheel
	cat ~/.pypi.cfg
	twine upload dist/*
