# coding: utf-8
""" Nested OBject manipulator """
from setuptools import find_packages, setup
import nob


with open("README.md", "r") as fin:
    readme = fin.read()

setup(
    name='nob',
    version=nob.__version__,
    description="Nested OBject manipulations",
    author_email="lapeyre@cerfacs.fr",
    url="https://gitlab.com/cerfacs/nob",
    long_description=readme,
    long_description_content_type="text/markdown",
    keywords=["JSON", "YAML", "Nested Object"],
    classifiers=[
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    install_requires=[],
    extras_require={
        "test": ["numpy"],
        "docs": ["sphinx", "myst-parser"],
    },
    packages=find_packages(exclude=("tests", "docs")),
    package_data={},
    entry_points={
        "console_scripts": [
        ]
    },
    include_package_data=False,
)
