.. nob documentation master file, created by
   sphinx-quickstart on Fri Sep 10 15:50:07 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
.. _index:

nob: the Nested OBject manipulator
==================================

Nested Objects (nobs, often referred to simply as JSON objects) appear in many
software workflows. They are combinations of key-value mappings (dicts) and
sequences (lists) of data. Other, more human readable formats than JSON are
useful to view this type of data, such as YAML:

.. code-block:: yaml

   shelters:
    kennel:
     dogs:
     - name: Rex
       age: 3
       alpha: False
     - name: Beethoven
       age: 8
       alpha: True

Their manipulation can become cumbersome in pure Python, in proportion to the
complexity of their structure. With `nob <https://gitlab.com/cerfacs/nob>`_,
accessing the first dog's name is done with either of:

.. code-block:: python

   first = n['dogs'][0]['name'][:]
   first = n.dogs[0].name[:]

and finding the alpha dog's name with:

.. code-block:: python

   alpha = [nv for nv in n.dogs if nv.alpha[:]][0].name[:]

The industry standard to deal with nobs is known as Object Relational Mapping
(`ORMs`_). In ORMs, Python objects are defined in code to hold the nob data as
attributes. This is a great abstraction, but in some cases nobs can have
complex, uncertain structure, and it can be very difficult to build and ORM for
them. Or, you just don't want to deal with building an ORM. This is where
:py:`nob` comes in.

.. _ORMs: https://en.wikipedia.org/wiki/Object%E2%80%93relational_mapping

:py:`nob` is a wrapper for nested objects that offers a set of functionalities to
simplify their direct manipulation. In doing so, it strives to stay as close as
possible to native python datatype interfaces, and to be intuitive for
developers with Python habits.


User's Guide
------------

:py:`nob` is meant to be intuitive, but there are some ideas you must have about
the underlying data model in order to not be surprised by its behaviour. This
part explores these ideas in both general and concrete form.

.. toctree::
   :maxdepth: 2

   philosophy
   quickstart
   tools
   numpy
   path
   cookbook

API Reference
-------------

You can dive into the specific functions of :py:`nob` here.

.. toctree::
   :maxdepth: 2

   api

Additional Notes
----------------

Miscellaneous information about the project.

.. toctree::
   :maxdepth: 2

   contributing
   license
   changes

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
