.. _api:

API
===

.. module:: nob

Here we describe in detail all public interfaces of :py:`nob`.


Nob Object
----------

:py:`Nob` is the central class of :py:`nob`. This is what is used to instatiate a nested object.

.. autoclass:: Nob
   :members:
   :inherited-members:


NobView Object
--------------

:py:`NobView` is designed to make deep tree operations transparent to the user. When you work on a
sub-tree, you are working on a :py:`NobView` object, even if you started from a :py:`Nob` one. This
view on the original tree has the same methods and behavior, and points to the same single object
in memory.

.. autoclass:: NobView
   :members:
   :inherited-members:


Path Object
-----------

:py:`Path` is a low-level object in :py:`nob`, which offers simplified representation and
manipulation of paths in the nested object.

.. autoclass:: Path
   :members:
   :inherited-members:


tools module
------------

Some additional tools are available:

.. automodule:: nob.tools
   :members:
