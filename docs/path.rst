.. _path:

Path
====

All paths are stored internally using the :py:`nob.Path` class. Paths are full
(w.r.t. their :py:`Nob` or :py:`NobView`), and are in essence a list of the keys
constituting the nested address. They can however be viewed equivalently as
a unix-type path string with :py:`/` separators. Here are some examples

.. code-block:: python

    p1 = Path(['key1'])
    p1                       >>> Path(/key1)
    p2 = Path('/key1/key2')
    p2                       >>> Path(/key1/key2)
    p1 / 'key3'              >>> Path(/key1/key3)
    p2.parent                >>> Path(/key1)
    p2.parent == p1          >>> True
    'key2' in p2             >>> True
    [k for k in p2]          >>> ['key1', 'key2']
    p2[-1]                   >>> 'key2'
    len(p2)                  >>> 2

These can be helpful to manipulate paths yourself, as any full access with a
string to a :py:`Nob` or :py:`NobView` object also accepts a :py:`Path` object.
So say you are accessing the keys in :py:`list_of_keys` at one position, but
that thet also exist elsewhere in the tree. You could use *e.g.*:

.. code-block:: python

    root = Path('/path/to/root/of/keys')
    [n[root / key] for key in list_of_keys]

.. warning::
    You might have noticed that :py:`Path` behaves similarly to the
    :py:`pathlib.PosixPath` object, and that's by design. However, it is *not*
    the same. Don't feed :py:`Nob` objects :py:`pathlib.PosixPath` objects,
    that won't go well.
