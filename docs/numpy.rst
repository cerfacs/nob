.. _numpy:

Numpy Specifics
===============

If you end up with numpy arrays in your tree, you are no longer JSON
compatible. You can remediate this by using the :py:`np.ndarray.tolist()` method,
but this can lead to a very long JSON file. To help you with this, Nob offers
the :py:`np_serialize` method, which efficiently rewrites all numpy arrays as
binary strings using the internal :py:`np.save` function. You can even compress
these using the standard zip algorithm by passing the :py:`compress=True`
argument. The result can be written directly to disc as a JSON or YAML file:

.. code-block:: python

    n.np_serialize()
    # OR
    n.np_serialize(compress=True)

    with open('file.json', 'w') as fh:
        json.dump(n[:], fh)
    # OR
    with open('file.yml', 'w') as fh:
        yaml.dump(n[:], fh)

To read it back, use the opposite function :py:`np_deserialize`:

.. code-block:: python

    with open('file.json') as fh:
        n = Nob(json.load(fh))
    # OR
    with open('file.yml') as fh:
        n = Nob(yaml.load(fh))
    n.np_deserialize()

And that's it, your original Nob has been recreated.

