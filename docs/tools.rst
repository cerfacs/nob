.. _tools:

Other Tools
===========

Below is a collection of miscellaneous tools that are available on any :py:`Nob`
or :py:`NobView` object.

Paths and Leaves
----------------

Any :py:`Nob` (or :py:`NobView`) object can introspect itself to find all its valid paths:

.. code-block:: python

    n.paths                  >>> [Path(/),
                                  Path(/city),
                                  Path(/kennel),
                                  Path(/kennel/name),
                                  Path(/kennel/employees),
                                  Path(/kennel/dogs),
                                  Path(/kennel/dogs/0),
                                  Path(/kennel/dogs/0/name),
                                  Path(/kennel/dogs/0/age),
                                  Path(/kennel/dogs/0/alpha),
                                  Path(/kennel/dogs/1),
                                  Path(/kennel/dogs/1/name),
                                  Path(/kennel/dogs/1/age),
                                  Path(/kennel/dogs/1/alpha)]

Alternatively, you can ask only for the leaves of the tree, *i.e.* the
paths to values which are neither dicts nor lists:

.. code-block:: python

    n.leaves                 >>> [Path(/kennel/name),
                                  Path(/kennel/employees),
                                  Path(/kennel/dogs/0/name),
                                  Path(/kennel/dogs/0/age),
                                  Path(/kennel/dogs/0/alpha),
                                  Path(/kennel/dogs/1/name),
                                  Path(/kennel/dogs/1/age),
                                  Path(/kennel/dogs/1/alpha)]

.. tip::

        These are lists of :py:`nob.Path` objects. For more on them, see the
        dedicated page: :ref:`path`.

Find
----

In order to easily search in this path list, the :py:`.find` method is available:

.. code-block:: python

    n.find('name')           >>> [Path(/kennel/name),
                                  Path(/kennel/dogs/0/name),
                                  Path(/kennel/dogs/1/name)]

The elements of these lists are not strings, but :py:`Path` objects, as
described in :ref:`path`.

.. _iteration:

Iteration
---------

Any tree or tree view is also iterable, yielding its children:

.. code-block:: python

    [nv for nv in n.kennel]    >>> [NobView:/kennel/name,
                                    NobView:/kennel/employees,
                                    NobView:/kennel/dogs]

.. warning::
    This feature can be counter-intuitive: iterating on a dict yields
    its keys, but on a list it yields its values. We choose here to
    behave uniformly like lists and always yield the values. To get
    the keys, use the :py:`.keys` method (see below).

Copy
----

To make an independant copy of a tree, use its :py:`.copy()` method:

.. code-block:: python

    n_cop = n.copy()
    n == n_cop               >>> True
    n_cop.city = 'London'
    n == n_cop               >>> False

A new standalone tree can also be produced from any tree view:

.. code-block:: python

    n_cop = n.city.copy()
    n_cop == n.city          >>> True
    n_cop.city = 'London'
    n_cop == n.city          >>> False


Keys
----

You can access keys at any level, just like in a dict:

.. code-block:: python

    n.kennel.keys()          >>> dict_keys(['name', 'employees', 'dogs'])
    n.dogs.keys()            >>> dict_keys([0, 1])


Comparisons
-----------

A :py:`numpy.allclose` clone is available for convenience, and can be applied to a pair
of :py:`Nob` objects:

.. code-block:: python

    allclose(a, b)           >>> True or False

If you wish to dig deeper into the differences between two :py:`Nob` objects, use :py:`diff`:

.. code-block:: python

    diff(a, b)               >>> ([Path('/only_in_a')], Path('/different_value'), Path('/only_in_b'))