from nob import Nob


t = Nob({
    'key1': 'val1', 
    'key2': {
        'key3': 4,
        'key4': {'key5': 'val2'},
        'key5': [3, 4, 5]
        },
    'key5': 'val3'
    })

t['/key1']         # NobVue(/key1)
t['/key1'][:]      # 'val1'
t.key1             # NobVue(/key1)
t.key1[:]          # 'val1'
t.key1 = 'new'     # updates t['/key1']
t['key2']          # NobVue(/key2)
t.key2             # NobVue(/key2)
t['/key5']         # NobVue(/key5)
t['key5']          # KeyError(non unique)
t.key5             # KeyError(non unique)
t.key4             # NobVue(/key2/key4)
t.key4 = 5         # Updates t['/key2/key4']
t.key2.key5.key5   # NobVue(/key2/key5/key5)
t.paths            # [Path(/key1), Path(/key2/key3/0), Path(/key2/key3/1),
                   #  Path(/key2/key3/1), Path(/key2/key4), Path(/key2/key5/key5),
                   #  Path(/key5)]
t.key2.paths       # [Path(/key3/0), Path(/key3/1), Path(/key3/1),
                   #  Path(/key4), Path(/key5/key5)]
t.find('key5')     # [Path(/key2/key5), Path(/key2/key5/key5), Path(/key5)]


import json
with open('file.json') as fh:
    t2 = Nob(json.load(fh))

import yaml
with open('file.yml') as fh:
    t3 = Nob(yaml.load(fh))