import nox

nox.options.default_venv_backend = "venv"


@nox.session
def tests(session):
    session.install(".[test]")
    session.install("pytest")
    session.run("pytest")


@nox.session
def lint(session):
    session.install("flake8")
    session.run("flake8", "nob")


@nox.session
def docs(session):
    session.install(".[docs]")
    session.chdir("docs")
    session.run("make", "html", external=True)
